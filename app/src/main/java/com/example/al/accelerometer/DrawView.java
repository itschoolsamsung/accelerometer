package com.example.al.accelerometer;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class DrawView extends SurfaceView implements SurfaceHolder.Callback {

    public Bitmap bitmap;

    public DrawThread drawThread;

    public DrawView(Context context) {
        super(context);

        getHolder().addCallback(this);
    }

    public DrawView(Context context, AttributeSet attrs) {
        super(context, attrs);

        getHolder().addCallback(this);
    }

    public DrawView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        getHolder().addCallback(this);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        drawThread = new DrawThread(getContext(), getHolder());
        drawThread.start();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        drawThread.requestStop();
        boolean retry = true;

        while (retry) {
            try {
                drawThread.join();
                retry = false;
            } catch (InterruptedException e) {
            }
        }
    }

    public class DrawThread extends Thread {

        private SurfaceHolder surfaceHolder;
        private volatile boolean running = true;
        private Paint paint = new Paint();
        private float radius = 50;
        private float x = 100;
        private float y = 100;
        private float dx = 0;
        private float dy = 0;
        private float maxD = 50;

        DrawThread(Context context, SurfaceHolder surfaceHolder) {
            this.surfaceHolder = surfaceHolder;
        }

        void moveLeft() {
            if (this.dx > -this.maxD) {
                --this.dx;
            }
        }

        void moveRight() {
            if (this.dx < this.maxD) {
                ++this.dx;
            }
        }

        void requestStop() {
            running = false;
        }

        @Override
        public void run() {
            while (running) {
                Canvas canvas = surfaceHolder.lockCanvas();
                if (canvas != null) {
                    try {
                        canvas.drawARGB(255, 0, 0, 50);
                        canvas.drawBitmap(bitmap, 0, 0, paint);
                        paint.setColor(Color.RED);
                        canvas.drawCircle(x, y, radius, paint);
                        x += dx;
                        y += dy;
                        if (x <= radius) {
                            x = radius;
                            dx = 0;
                        }
                        if (x >= canvas.getWidth() - radius) {
                            x = canvas.getWidth() - radius;
                            dx = 0;
                        }
                        if (y <= radius) {
                            y = radius;
                            dy = 0;
                        }
                        if (y >= canvas.getHeight() - radius) {
                            y = canvas.getHeight() - radius;
                            dy = 0;
                        }
                    } finally {
                        surfaceHolder.unlockCanvasAndPost(canvas);
                    }
                }
            }
        }
    }
}
