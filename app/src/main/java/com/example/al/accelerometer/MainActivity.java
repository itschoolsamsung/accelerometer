package com.example.al.accelerometer;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;

public class MainActivity extends Activity implements SensorEventListener {

    SensorManager mSensorManager;
    Sensor mAccelerometer;
    DrawView drawView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        drawView = findViewById(R.id.drawView);
        drawView.bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.ic_sochi);
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        drawView.bitmap = Bitmap.createScaledBitmap(drawView.bitmap, size.x, size.y, false);

        mSensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
    }

    @Override
    protected void onResume() {
        super.onResume();

        mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onPause() {
        super.onPause();

        mSensorManager.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        Log.i(
            "_SENSOR_",
            event.values[0] + " : " + event.values[1] + " : " + event.values[2]
        );

        if (event.values[0] > 0.5) {
            if (drawView.drawThread != null) {
                drawView.drawThread.moveLeft();
            }
        }

        if (event.values[0] < -0.5) {
            if (drawView.drawThread != null) {
                drawView.drawThread.moveRight();
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }
}
